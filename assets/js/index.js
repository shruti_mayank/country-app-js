const countryList = document.getElementById('country');
const searchBar = document.getElementById('search');
let countryCards = [];

searchBar.addEventListener('keyup', (e) =>{
    const searchCountry = e.target.value.toLowerCase();
    const filtercountry = countryCards.filter((country)=>{  
        return(
            country.name.toLowerCase().includes(searchCountry)
        );
    });
    displayCountries(filtercountry);
})

const loadCountry = async() => {
    try
    {
        const res = await fetch('https://restcountries.eu/rest/v2/all');
        countryCards = await res.json();
        displayCountries(countryCards);
    }
    catch(err)
    {
        console.log(err);
    }
};

const displayCountries = (countries) => {
    const countryString = countries
    .map((country)=>{
        return `<div class="card mt-4 p-3"> 
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="${country.flag}" class="h-100 w-100 img-fluid img-responsive" alt="">
                        </div>
                        <div class="col-sm-8">
                            <h3 class="pt-2">${country.name}</h3>
                            <p>
                                <span>Currency: ${country.currencies[0].name}</span>
                                <span>Current date and time: ${country.timezones}</span>
                            </p>
                            <a href="https://maps.google.com/?q=${country.latlng}" target="_blank"><button class="btn btn-outline-primary border-2 btn1">Show Map</button></a>
                            <a href="detail.html?code=${country.alpha3Code}"><button class="btn btn-outline-primary border-2" id="detailBtn" value="${country.alpha3Code}">Detail</button></a>
                        </div>
                    </div>
                </div>`
    })
    .join('');
    countryList.innerHTML = countryString;
};

loadCountry();

