// var code = document.getElementById("detailBtn").val();
var code = window.location.search.replace('?','').split('&').find((o) => { return o.split('=').length > 0 && o.split('=')[0] === 'code' }).split('=')[1];

const loadFlag = (border)  => {
    var neighbour = document.getElementById('neighbourFlag');
    const flags = border
    .map((countryFlag)=>{
        fetch(`https://restcountries.eu/rest/v2/alpha/${countryFlag}`)
        .then((res)=>{
            return res.json();
        })
        .then((countryFlags)=>{
            console.log(countryFlags.flag)
            return `<div class="col-md-4">
                        <img src="${countryFlags.flag}" class="img-fluid h-100 img-responsive" alt=""></img>
                    </div>`
        })
    })
    .join('');
    console.log(flags)
    neighbour.innerHTML = flags;
};

fetch(`https://restcountries.eu/rest/v2/alpha/${code}`)
.then((apiData) =>{
    return apiData.json();
})
.then((countryData) =>{
    var details = document.getElementById('detailPage');
    details.innerHTML = `<h1>${countryData.name}</h1>
                            <div class="row py-3">
                                <div class="col-md-5">
                                    <img src="${countryData.flag}" class="img-fluid img-responsive" alt="">
                                </div>
                                <div class="col-md-7">
                                    <div class="px-md-5">
                                        <span>Native Name: ${countryData.nativeName}</span>
                                        <span>Capital: ${countryData.capital}</span>
                                        <span>Population: ${countryData.population}</span>
                                        <span>Region: ${countryData.region}</span>
                                        <span>Sub-region: ${countryData.subregion}</span>
                                        <span>Area: ${countryData.area}</span>
                                        <span>Country code: ${countryData.callingCodes}</span>
                                        <span>Languages: ${countryData.languages.map(({name})=>name).join(", ")}</span>
                                        <span>Currencies: ${countryData.currencies.map(({name})=>name).join(", ")}</span>
                                        <span>Timezones: ${countryData.timezones}</span>
                                    </div>
                                </div>
                            </div>`
        // console.log(countryData.borders)
        loadFlag(countryData.borders);
})
.catch(error)
{
    console.log(error);
}